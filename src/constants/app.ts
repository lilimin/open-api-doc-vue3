import type { Swagger } from './swagger'

export interface Tag {
    name: string
    paths: { [name: string]: Swagger.Path3 }
}
export interface OriginApiGroup {
    name: string
    link: string
}

/**
 * 全局参数
 */
export interface GlobalParam {
    name: string
    in: 'query' | 'header'
    value: any
}
